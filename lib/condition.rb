#!/usr/bin/env ruby

require 'nokogiri'
require 'open-uri'

class Condition
	attr_reader :condition, :sub_condition

	def initialize(condition_html)
		@condition_html = condition_html
	end

	def condition
		parse_condition() if @condition.nil?
		@condition
	end

	def sub_condition
		parse_condition() if @sub_condition.nil?
		@sub_condition
	end

	def parse_condition
		doc = (@condition_html.is_a? (Nokogiri::XML::Element)) ? @condition_html : (Nokogiri::HTML(@condition_html))

		# parse condition sub_condition
		condition = doc.at_css("span[class='a-size-medium olpCondition a-text-bold']").text.gsub(/[\s]/, '')
		if condition.include?"-"
			@condition = condition.split('-')[0]
			@sub_condition = condition.split('-')[-1]
		else
			@condition = condition
			@sub_condition = nil
		end

		# p @condition
		# p @sub_condition
	end

end
