#!/usr/bin/env ruby

require 'nokogiri'
require 'open-uri'
require 'seller'
require 'condition'
require 'price'
# require_relative '../lib/seller.rb'
# require_relative '../lib/condition.rb'
# require_relative '../lib/price.rb'

class Offer
	attr_reader :seller, :price, :condition
	def initialize(offer_html)
		@offer_html = offer_html
	end

	def seller
		parse_offer() if @seller.nil?
		@seller
	end

	def price
		parse_offer() if @price.nil?
		@price
	end

	def condition
		parse_offer() if @condition.nil?
		@condition
	end	

	def parse_offer()
		@seller = Seller.new(@offer_html)
		# p @seller.seller_name
		# p @seller.seller_id
		# p @seller.feedback
		# p @seller.ratings

		@price = Price.new(@offer_html)
		# p @price.price
		# p @price.shipping_fee	

		@condition = Condition.new(@offer_html)
		# p @condition.condition
		# p @condition.sub_condition

	end
end
