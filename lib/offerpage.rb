#!/usr/bin/env ruby

require 'nokogiri'
require 'open-uri'
require 'offer'
# require_relative '../lib/offer.rb'

class Offerpage
	attr_reader :offer_attr

	def initialize(offerpage_html)
		@offerpage_html = offerpage_html
	end

	def offer_attr
		parse_offerpage() if @offer_attr.nil?
		@offer_attr
	end

	def parse_offerpage()
		doc = Nokogiri::HTML( File.open(@offerpage_html) )
		@offer_attr = doc.css("div[class='a-row a-spacing-mini olpOffer']")
		offer_attr.each do |offer_html|
			offer = Offer.new(offer_html)
			p "seller_name: #{offer.seller.seller_name}"
			p "seller_id: #{offer.seller.seller_id}"
			p "feedback: #{offer.seller.feedback}"
			p "ratings: #{offer.seller.ratings}"
			p "price: #{offer.price.price}"
			p "shipping_fee: #{offer.price.shipping_fee}"	
			p "condition: #{offer.condition.condition}"
			p "sub_condition: #{offer.condition.sub_condition}"
			p "---------------------------------------"
		end
	end
end

