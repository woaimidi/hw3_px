#!/usr/bin/env ruby

require 'nokogiri'
require 'open-uri'

class Price
	attr_reader :price, :shipping_fee
	def initialize(price_html)
		@price_html = price_html
	end

	def price
		parse_price() if @sale_price.nil?
		@price
	end

	def shipping_fee
		parse_price() if @shipping_fee.nil?
		@shipping_fee
	end

	def parse_price
		doc = (@price_html.is_a? (Nokogiri::XML::Element)) ? @price_html : (Nokogiri::HTML(@price_html))

		# parse price
		@price = doc.at_css("span[class='a-size-large a-color-price olpOfferPrice a-text-bold']").text.gsub(/[\s]/, '')
		# p @price

		# parse shipping_fee
		shippingPrice = doc.at_css("span[class='olpShippingPrice']")
		if shippingPrice.class == NilClass
			@shipping_fee = "FREE Shipping"
		else
			@shipping_fee = shippingPrice.text.gsub(/[\s]/, '')		
		end
		# p @shipping_fee
	end

end

# price_without_shippingfee_html = '<div class="a-column a-span2 olpPriceColumn" role="gridcell">
#   	<span class="a-size-large a-color-price olpOfferPrice a-text-bold">                $1,059.00                </span>
#                 <p class="olpShippingInfo">
#                    <span class="a-color-secondary">
#                 &amp; <b>FREE Shipping</b>
#         <span class="olpEstimatedTaxText">
#             + $0.00 estimated tax
#         </span>
#         </span>
#     </p>
#        <div><div id="keepaMAP" style="font-size: 12px; color: rgb(153, 153, 153);">Stock revealed by Keepa:</div><div>
#   Only 1 left in stock.
# 	</div>'
# test = Price.new(price_without_shippingfee_html)
# test.parse_price
# price_with_shippingfee_html = '<div class="a-column a-span2 olpPriceColumn" role="gridcell">
#     <span class="a-size-large a-color-price olpOfferPrice a-text-bold">                $1,087.99                </span>
#     <p class="olpShippingInfo">
#         <span class="a-color-secondary">
#             + <span class="olpShippingPrice">$8.99</span>
#             <span class="olpShippingPriceText">shipping</span>
#             <span class="olpEstimatedTaxText">
#                 + $0.00 estimated tax
#             </span>
#         </span>
#     </p>
#     <div><div id="keepaMAP" style="font-size: 12px; color: rgb(153, 153, 153);">Stock revealed by Keepa:</div><div>
#   Only 2 left in stock.
# 	</div>'
# test = Price.new(price_with_shippingfee_html)
# test.parse_price