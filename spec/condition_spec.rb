require 'condition'

describe Condition do
	condition_without_sub_condition_html = '<span id="olpRefurbished" class="a-size-medium olpCondition a-text-bold">
    Refurbished
    
	</span>'
	condition_with_sub_condition_html = '<span id="olpUsed" class="a-size-medium olpCondition a-text-bold">
    Used
    



	-
	<span id="offerSubCondition">
	    Acceptable
	</span>
	</span>'

	# condition_without_sub_condition_html test
	condition_without_sub_condition = Condition.new(condition_without_sub_condition_html)
	it "parse condition_without_sub_condition_html" do
		expect(condition_without_sub_condition.condition).to eq("Refurbished")
		expect(condition_without_sub_condition.sub_condition).to eq(nil)
		p "parse condition_without_sub_condition_html:"
		p "condition: %s" %condition_without_sub_condition.condition
		p "sub_condition: nil" 
		p "-------------------------------------"
	end

	# condition_with_sub_condition_html test
	condition_with_sub_condition = Condition.new(condition_with_sub_condition_html)
	it "parse condition_with_sub_condition_html" do
		expect(condition_with_sub_condition.condition).to eq("Used")
		expect(condition_with_sub_condition.sub_condition).to eq("Acceptable")
		p "parse condition_with_sub_condition_html:"
		p "condition: %s" %condition_with_sub_condition.condition
		p "sub_condition: %s" %condition_with_sub_condition.sub_condition
		p "-------------------------------------"
	end

end