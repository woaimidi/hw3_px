require 'offer'

describe Offer do
	offer_html = '<div class="a-row a-spacing-mini olpOffer" role="row">
                            <div class="a-column a-span2 olpPriceColumn" role="gridcell">
                                







        
        



    
    

      
      
      
      
  
    
  
    
  
  
  
      
      
      
      
      
  
      
      
      
    
      
    
    
    
  
    
    
  




                    <span class="a-size-large a-color-price olpOfferPrice a-text-bold">                $1,098.51                </span>






















                <p class="olpShippingInfo">
                   <span class="a-color-secondary">




                &amp; <b>FREE Shipping</b>
        <span class="olpEstimatedTaxText">
            + $0.00 estimated tax
        </span>

        </span>
    </p>



        










    


        
    
    








    
    
            


                            </div>
                            <div class="a-column a-span3 olpConditionColumn" role="gridcell">
                                



















        
        
        <div id="offerCondition" class="a-section a-spacing-small">
        




<span id="olpUsed" class="a-size-medium olpCondition a-text-bold">
    Used
    



-
<span id="offerSubCondition">
    Good
</span>
</span>

        
    </div>


    


    <div class="comments" style="display: inline-block;">
        <div class="expandedNote" style="display: none;">
             90 DAY WARRANTY - 100% Fully Functional. MacBooks are professionally restored, sanitized and inspected by AUTHORIZED CERTIFIED TECHNICIANS. We also offer FREE technical repairs on any device purchased included in our 90-day warranty. Overall GOOD condition. MacBook is Pre-Owned and will come with signs of use including but not limited to scratches, scuffs, dented corners and possible wear on LCD from keyboard. LCD may also have dead pixels. OS X is not guaranteed, may come installed with older o
             <span style="white-space: nowrap">
                 <span class="caretback">«</span>
                 <a href="javascript:void(0)" class="toggleNote">Show less</a>
             </span>
        </div>
        <div class="collapsedNote" style="display: block;">
             90 DAY WARRANTY - 100% Fully Functional. MacBooks are professiona...
             <span style="white-space: nowrap">
                 <span class="caretnext">»</span>
                 <a href="javascript:void(0)" class="toggleNote">Read more</a>
             </span>
        </div>
    </div>

    <noscript>
    <div class="comments">
            90 DAY WARRANTY - 100% Fully Functional. MacBooks are professionally restored, sanitized and inspected by AUTHORIZED CERTIFIED TECHNICIANS. We also offer FREE technical repairs on any device purchased included in our 90-day warranty. Overall GOOD condition. MacBook is Pre-Owned and will come with signs of use including but not limited to scratches, scuffs, dented corners and possible wear on LCD from keyboard. LCD may also have dead pixels. OS X is not guaranteed, may come installed with older o
    </div>
    </noscript>


    







     

                            </div>
                            <div class="a-column a-span3 olpDeliveryColumn" role="gridcell">
                                











        


    <p class="a-spacing-mini olpAvailability">
        




    


        
</p><ul class="a-unordered-list a-vertical olpFastTrack">
            <li><span class="a-list-item">
                    <div aria-live="polite" data-a-expander-collapsed-height="60" class="a-expander-collapsed-height a-row a-expander-container a-spacing-base a-expander-partial-collapse-container olpAvailabilityExpander" style="max-height:60px; _height:60px">
                        <div aria-expanded="false" class="a-expander-content a-expander-partial-collapse-content">
                            <b>Arrives between</b> December 18-20.
                        </div>
                        <div class="a-expander-header a-expander-partial-collapse-header" style="opacity: 0; display: none;"><div class="a-expander-content-fade"></div><a href="javascript:void(0)" data-action="a-expander-toggle" class="a-declarative" data-a-expander-toggle="{&quot;allowLinkDefault&quot;:true, &quot;expand_prompt&quot;:&quot;Read more&quot;, &quot;collapse_prompt&quot;:&quot;Show less&quot;}"><i class="a-icon a-icon-extender-expand"></i><span class="a-expander-prompt">Read more
                        </span></a></div>
                    </div>
            </span></li>



        



        




        <li><span class="a-list-item">
            Ships from CA, United States.
        </span></li>
        <li><span class="a-list-item">
            <a href="/gp/aag/details/ref=olp_merch_ship_1?ie=UTF8&amp;asin=B00XZGMBVC&amp;seller=A25AYDEDD8D7VG&amp;sshmPath=shipping-rates#aag_shipping">Shipping rates</a>
                   and <a href="/gp/aag/details/ref=olp_merch_return_1?ie=UTF8&amp;asin=B00XZGMBVC&amp;seller=A25AYDEDD8D7VG&amp;sshmPath=returns#aag_returns">return policy</a>.
        </span></li>
</ul>




    <p></p>


                            </div>
                            <div class="a-column a-span2 olpSellerColumn" role="gridcell">
                                







    








<h3 class="a-spacing-none olpSellerName">



        <span class="a-size-medium a-text-bold">            <a href="/gp/aag/main/ref=olp_merch_name_1?ie=UTF8&amp;asin=B00XZGMBVC&amp;isAmazonFulfilled=0&amp;seller=A25AYDEDD8D7VG">JemJem Remagic</a>        </span>
</h3>






                <p class="a-spacing-small">
                            <i class="a-icon a-icon-star a-star-4-5"><span class="a-icon-alt">4.5 out of 5 stars</span></i>
                            <a href="/gp/aag/main/ref=olp_merch_rating_1?ie=UTF8&amp;asin=B00XZGMBVC&amp;isAmazonFulfilled=0&amp;seller=A25AYDEDD8D7VG"><b>94% positive</b></a> over the past 12 months. (1,877 total ratings)
                        <br>
                </p>



        





                            </div>
                            <div class="a-column a-span2 olpBuyColumn a-span-last" role="gridcell">
                                








    
  




<div class="a-button-stack">


                  <form method="post" action="/gp/item-dispatch/ref=olp_atc_used_31" class="a-spacing-none">
                  <input type="hidden" name="session-id" value="136-2902209-5608355">
                  <input type="hidden" name="qid">
                  <input type="hidden" name="sr">
                  <input type="hidden" name="signInToHUC" value="0" id="signInToHUC">
                  <input type="hidden" name="metric-asin.B00XZGMBVC" value="1">
                  <input type="hidden" name="registryItemID.1">
                  <input type="hidden" name="registryID.1">
                          <input type="hidden" name="itemCount" value="1">
                  <input type="hidden" name="offeringID.1" value="Xod3GgiQOP91%2BmluLEogQYKMH3OB%2FoXavXPsGSwXzWV3RvxdBuKTSwdbE9a4Ik%2FyAacE%2FNeNtpnskt3WzHgM0y7ke%2FzEjkYVsI4TKlAH24HKYNpC4DUpqkibM6PcVHIEc4GSQxzQC0gDmefTsHh2x8n2DUPQwytA">
                  <input type="hidden" name="isAddon" value="1">



                  <span class="a-declarative" data-action="olp-click-log" data-olp-click-log="{&quot;subtype&quot;:&quot;main&quot;,&quot;type&quot;:&quot;addToCart&quot;}">
                             <span class="a-button a-button-normal a-spacing-micro a-button-primary a-button-icon" id="a-autoid-2"><span class="a-button-inner"><i class="a-icon a-icon-cart"></i><input name="submit.addToCart" class="a-button-input" type="submit" value="Add to cart" aria-labelledby="a-autoid-2-announce"><span class="a-button-text" aria-hidden="true" id="a-autoid-2-announce">
                                Add to cart
                                <span class="a-offscreen" "="">from seller JemJem Remagic and price $1,098.51</span>
                             </span></span></span>
                  </span>



          </form>




</div>




                            </div>
                  </div>'

	# offer_html test
	offer = Offer.new(offer_html)
	it "parse offer_html" do
		expect(offer.seller.seller_name).to eq("JemJem Remagic")
		expect(offer.seller.seller_id).to eq("A25AYDEDD8D7VG")
		expect(offer.seller.feedback).to eq("94")
		expect(offer.seller.ratings).to eq("1877")
		expect(offer.price.price).to eq("$1,098.51")
		expect(offer.price.shipping_fee).to eq("FREE Shipping")
		expect(offer.condition.condition).to eq("Used")
		expect(offer.condition.sub_condition).to eq("Good")

		p "parse offer-normal.html:"
		p "seller_name: %s" %offer.seller.seller_name
		p "seller_id: %s" %offer.seller.seller_id
		p "feedback: %s" %offer.seller.feedback
		p "ratings: %s" %offer.seller.ratings
		p "price: %s" %offer.price.price
		p "shipping_fee: %s" %offer.price.shipping_fee
		p "condition: %s" %offer.condition.condition
		p "sub_condition: %s" %offer.condition.sub_condition
		p "-------------------------------------"
	end
end