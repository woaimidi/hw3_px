require 'price'

describe Price do
	price_without_shippingfee_html = '<div class="a-column a-span2 olpPriceColumn" role="gridcell">
  	<span class="a-size-large a-color-price olpOfferPrice a-text-bold">                $1,059.00                </span>
                <p class="olpShippingInfo">
                   <span class="a-color-secondary">
                &amp; <b>FREE Shipping</b>
        <span class="olpEstimatedTaxText">
            + $0.00 estimated tax
        </span>
        </span>
    </p>
       <div><div id="keepaMAP" style="font-size: 12px; color: rgb(153, 153, 153);">Stock revealed by Keepa:</div><div>
  Only 1 left in stock.
	</div>'
    price_with_shippingfee_html = '<div class="a-column a-span2 olpPriceColumn" role="gridcell">
    <span class="a-size-large a-color-price olpOfferPrice a-text-bold">                $1,087.99                </span>
    <p class="olpShippingInfo">
        <span class="a-color-secondary">
            + <span class="olpShippingPrice">$8.99</span>
            <span class="olpShippingPriceText">shipping</span>
            <span class="olpEstimatedTaxText">
                + $0.00 estimated tax
            </span>
        </span>
    </p>
    <div><div id="keepaMAP" style="font-size: 12px; color: rgb(153, 153, 153);">Stock revealed by Keepa:</div><div>
  Only 2 left in stock.
	</div>'

	# price_without_shippingfee_html test
	price_without_shippingfee = Price.new(price_without_shippingfee_html)
	it "parse price_without_shippingfee_html" do
		expect(price_without_shippingfee.price).to eq("$1,059.00")
		expect(price_without_shippingfee.shipping_fee).to eq("FREE Shipping")
		p "parse price_without_shippingfee_html:"
		p "price: %s" %price_without_shippingfee.price
		p "shipping_fee: %s" %price_without_shippingfee.shipping_fee
		p "-------------------------------------"
	end

	# price_with_shippingfee_html test
	price_with_shippingfee = Price.new(price_with_shippingfee_html)
	it "parse price_with_shippingfee_html" do
		expect(price_with_shippingfee.price).to eq("$1,087.99")
		expect(price_with_shippingfee.shipping_fee).to eq("$8.99")
		p "parse price_with_shippingfee_html:"
		p "price: %s" %price_with_shippingfee.price
		p "shipping_fee: %s" %price_with_shippingfee.shipping_fee
		p "-------------------------------------"
	end

end