require 'seller'

describe Seller do

	# seller_normal_html = "seller-normal.html"
	# seller_just_launched_html = "seller-just-launched.html"
	# seller_amazon_html = "seller-amazon.html"
	seller_normal_html = '<div class="a-column a-span2 olpSellerColumn" role="gridcell"> 
  <h3 class="a-spacing-none olpSellerName"> 
    <span class="a-size-medium a-text-bold"> <a href="/gp/aag/main/ref=olp_merch_name_2?ie=UTF8&amp;asin=0761455299&amp;isAmazonFulfilled=0&amp;seller=A19A29L7PVBL05">D M Books Store</a> </span>
  </h3> 
  <p class="a-spacing-small"> <i class="a-icon a-icon-star a-star-4-5"><span class="a-icon-alt">4.5 out of 5 stars</span></i> <a href="/gp/aag/main/ref=olp_merch_rating_2?ie=UTF8&amp;asin=0761455299&amp;isAmazonFulfilled=0&amp;seller=A19A29L7PVBL05"><b>92% positive</b></a> over the past 12 months. (409 total ratings) <br> </p> 
</div>'
	seller_just_launched_html = '<div class="a-column a-span2 olpSellerColumn" role="gridcell">
  <h3 class="a-spacing-none olpSellerName"><span class="a-size-medium a-text-bold"><a href="/gp/aag/main/ref=olp_merch_name_7?ie=UTF8&amp;asin=0545261244&amp;isAmazonFulfilled=0&amp;seller=AYXDDSK9W0RO9">Kathysshop</a></span>
  </h3>
  <p class="a-spacing-small"><b class="olpJustLaunched">Just Launched</b>(<span class="olpSellerProfile"><a href="/gp/aag/main/ref=olp_merch_new_seller_7?ie=UTF8&amp;asin=0545261244&amp;isAmazonFulfilled=0&amp;seller=AYXDDSK9W0RO9">Seller Profile</a></span>)</p>
</div>'
	seller_amazon_html = '<div class="a-column a-span2 olpSellerColumn" role="gridcell">
  <h3 class="a-spacing-none olpSellerName"><img alt="Amazon.com" src="https://images-na.ssl-images-amazon.com/images/I/01dXM-J1oeL.gif">
  </h3>
</div>'

	# seller_normal_html test
	seller = Seller.new(seller_normal_html)
	it "parse seller_normal_html" do
		expect(seller.seller_name).to eq("D M Books Store")
		expect(seller.seller_id).to eq("A19A29L7PVBL05")
		expect(seller.feedback).to eq("92")
		expect(seller.ratings).to eq("409")
		p "parse seller-normal.html:"
		p "seller_name: %s" %seller.seller_name
		p "seller_id: %s" %seller.seller_id
		p "feedback: %s" %seller.feedback
		p "ratings: %s" %seller.ratings
		p "-------------------------------------"
	end

	# seller_just_launched_html test
	seller_just_launched = Seller.new(seller_just_launched_html)
	it "parse seller_just_launched_html" do
		expect(seller_just_launched.seller_name).to eq("Kathysshop")
		expect(seller_just_launched.seller_id).to eq("AYXDDSK9W0RO9")
		expect(seller_just_launched.feedback).to eq(nil)
		expect(seller_just_launched.ratings).to eq(nil)
		p "parse seller-just-launched.html:"
		p "seller_name: %s" %seller.seller_name
		p "seller_id: %s" %seller.seller_id
		p "feedback: nil" 
		p "ratings: nil"
		p "-------------------------------------"
	end

	# seller_amazon_html test
	seller_amazon = Seller.new(seller_amazon_html)
	it "parse seller_amazon_html" do
		expect(seller_amazon.seller_name).to eq("Amazon")
		expect(seller_amazon.seller_id).to eq(nil)
		expect(seller_amazon.feedback).to eq(nil)
		expect(seller_amazon.ratings).to eq(nil)
		p "parse seller-amazon.html:"
		p "seller_name: %s" %seller_amazon.seller_name
		p "seller_id: nil"
		p "feedback: nil" 
		p "ratings: nil"
		p "-------------------------------------"
	end

end